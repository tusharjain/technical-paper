# DOM - Document Object Model

## What is DOM?
DOM or Document Object Model - is data structure which a browser prepares for every web page that is loaded onto it. <br>
The data structure used here is a tree which make it easier to access and add individual elements than linear data structures. Also, it establishes the parent-child relation between the various elements nested within another element. <br>
Each branch ends in a node and each node contains objects.<br>
<br>
The document here refers to the built-in object which containers several methods and properties to access the DOM.

## How does it help?
This data structure provides access to manipulating the objects or nodes of the web page. This manipulation, via a programming language, is used to add interactivity to the page. This interactivity is the feedback (visual or otherwise) for user actions. 

*For example*, <br> 
- The filters used by e-commerce websites read the user-input and the web page makes the appropriate changes to displayed items. 
- DOM is also used by AdBlockers to hide ad elements from the webpage. 

## How to access it?
Consider the following example -
```
<html>
    <head>
        <title>Accessing the DOM</title>
        <style>
          div {
                border: 1px solid; 
          }

          .two.child {
            margin: 5px;
            width: 50%;
          }

          p {
            margin: 3px;
          }
        </style>
        <script src="test.js" defer></script>
    </head>
    <body>
        <main>
          <div id="one">
            <p><b>First div</b></p>
          </div>
          <div class="two">
              <p><b>Second div</b></p>
              <div class="two child"><b>inside second div 1</b></div>
              <div class="two child"><b>inside second div 2</b></div>
          </div>
          <div>
              <p><b>Third div</b></p>
          </div>
        </main>
    </body>
</html>

```
![image-1.png](https://gitlab.com/tusharjain/technical-paper/-/raw/master/image-1.png)

We will try to access the `div`s using the following methods - 

1. ***getElementById()***

   This method uses the id attribute of the element to access it. 
   Adding the following code in the script file
   ```
    document.getElementById('one').textContent += 'Accessed by getElementById()'

    ```

2. ***getElementsByClassName()***

   This method uses the class attribute of the element to access it. 
   We get a nodeList in return and so each node (element) can accessed using the index notation  
   similar to array. (since it is possible for several elements to use the same class)

   ```
   document.getElementsByClassName('two')[0].innerHTML += '<p>Accessed by getElementsByClass()</p>'
   ```
3. ***getElementsByTagName()***

   This method uses the name of the tag or element. 
   We get a nodeList of all the instances of the tag in return.
   Using `for` loop we access each of the nodes. 
   ```
    let x = document.getElementsByTagName('div');
    for(let element of x) {
        element.innerHTML += '<p>Accessed by getElementsByTagName()</p>'
    }
   ```
4. ***querySelector()***

   This method uses [CSS selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors) (not limited to using a *single* id, class, or type) and returns the ***first*** element that matches it. 

   ```
   document.querySelector('.two.child').innerHTML += '<p>Accessed by querySelector()</p>'
   ```
5. ***querySelectorAll()***

   This method also makes use of all the [CSS selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors) at disposal. It returns ***all*** the elements that match the selector. 

   ```
   let y = document.querySelectorAll('.two.child'); 
    for(let element of y) {
        element.innerHTML += `<p>Accessed by querySelectorAll()</p>`
    }
    ```

And the result is 
![image.png](https://gitlab.com/tusharjain/technical-paper/-/raw/a8e67a49f20ace8a93ea2c81c463e1a1998f105f/image.png)




*References:*<br>
https://www.digitalocean.com/community/tutorials/how-to-access-elements-in-the-dom<br>
https://eloquentjavascript.net/14_dom.html



